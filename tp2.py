# -*- coding: utf-8 -*-
"""
Created on Wed Nov 29 11:17:28 2017

@author: Daniel
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import silhouette_score, adjusted_rand_score
from sklearn import cluster, mixture
from sklearn.neighbors import KNeighborsClassifier
from skimage.io import imsave,imread
from matplotlib.font_manager import FontProperties

RADIUS = 6371

def plot_classes(labels,lon,lat, fileName='map.png', alpha=0.5, edge = 'k'):
    """Plot seismic events using Mollweide projection.
    Arguments are the cluster labels and the longitude and latitude
    vectors of the events"""
    img = imread("Mollweide_projection_SW.jpg")        
    plt.figure(figsize=(10,5),frameon=False)    
    x = lon/180*np.pi
    y = lat/180*np.pi
    ax = plt.subplot(111, projection="mollweide")
    #print(ax.get_xlim(), ax.get_ylim())
    t = ax.transData.transform(np.vstack((x,y)).T)
    #print(np.min(np.vstack((x,y)).T,axis=0))
    #print(np.min(t,axis=0))
    clims = np.array([(-np.pi,0),(np.pi,0),(0,-np.pi/2),(0,np.pi/2)])
    lims = ax.transData.transform(clims)
    plt.close()
    plt.figure(figsize=(10,5),frameon=False)    
    plt.subplot(111)
    plt.imshow(img,zorder=0,extent=[lims[0,0],lims[1,0],lims[2,1],lims[3,1]],aspect=1)        
    x = t[:,0]
    y= t[:,1]
    nots = np.zeros(len(labels)).astype(bool)
    diffs = np.unique(labels)    
    ix = 0   
    for lab in diffs[diffs>=0]:        
        mask = labels==lab
        nots = np.logical_or(nots,mask)        
        plt.plot(x[mask], y[mask],'o', markersize=4, mew=1,zorder=1,alpha=alpha, markeredgecolor=edge)
        ix = ix+1                    
    mask = np.logical_not(nots)    
    if np.sum(mask)>0:
        plt.plot(x[mask], y[mask], '.', markersize=1, mew=1,markerfacecolor='w', markeredgecolor=edge)
    plt.axis('off')
    plt.savefig(fileName,dpi=300)

fontP = FontProperties()
fontP.set_size('small')
def graph_validation_indexes(clusterParam,clusterParamLabel,validIndexes,delta=5,fileName='graph.png'):
    """Plot the graph for the validation indexes in function of the clustering parameter"""
    plt.figure(figsize=(20,10),frameon=False)
    ax = plt.subplot(111)
    
    # Plots
    ax.plot(clusterParam, validIndexes['precision'],'k', label='Precision', marker='o')
    ax.plot(clusterParam, validIndexes['recall'],'y', label='Recall', marker='D')
    ax.plot(clusterParam, validIndexes['rand'],'m', label='Rand', marker='^')
    ax.plot(clusterParam, validIndexes['f1'],'c', label='F1', marker='h')
    ax.plot(clusterParam, validIndexes['ari'],'r', label='ARI', marker='x')
    ax.plot(clusterParam, validIndexes['silhouette'],'g', label='Silhouette', marker='s')
    # Labels and title
    plt.xlabel(clusterParamLabel)
    plt.xticks(clusterParam[::delta]) # skip and only show every 5 ticks
    plt.ylabel('Validation Indexes')
    plt.grid(b=True, which='major', axis='both')
    #plt.title('Title')
    # Shrink current axis's height by 10% on the bottom
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.15, box.width, box.height * 0.9])
    # Put a legend below current axis
    legend = ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.12), fancybox=True, ncol=3, prop=fontP)
    # Show and save image
    plt.savefig(fileName,dpi=300, bbox_extra_artists=(legend,))
    plt.show()

def lat_long_to_3D(latitude, longitude):
    """Transform the latitude and longitude coordinates into
    Earth-centered, Earth-fixed coordinates (x,y,z)"""
    x = RADIUS * np.cos(latitude * np.pi/180) * np.cos(longitude * np.pi/180)
    y = RADIUS * np.cos(latitude * np.pi/180) * np.sin(longitude * np.pi/180)
    z = RADIUS * np.sin(latitude * np.pi/180)
    return x,y,z

def calc_confusion_matrix(numPoints, faults, labels):
    """Calculate the confusion matrix for given faults and labels
    Returns number of true positives/negatives and false positives/negatives"""
    
    # Get matrices that tell if a pair of points are in the same fault/cluster
    equal_fault = np.zeros((numPoints,numPoints), dtype=bool)
    equal_cluster = np.zeros((numPoints,numPoints), dtype=bool)
    for i in range(numPoints):
        equal_fault[i] = faults[i] == faults[:]
        equal_cluster[i] = labels[i] == labels[:]
    
    # Determine TP, TN, FP and FN
    numTP = 0
    numTN = 0
    numFP = 0
    numFN = 0
    for i in range(numPoints):
        and_AB = np.logical_and(equal_fault[i,i+1:], equal_cluster[i,i+1:]) # AND(A,B)
        or_AB = np.logical_or(equal_fault[i,i+1:], equal_cluster[i,i+1:]) # OR(A,B)
        nor_AB = np.logical_not(or_AB) # NOT(OR(A,B))
        xor_AB = np.logical_xor(equal_fault[i,i+1:], equal_cluster[i,i+1:]) # XOR(A,B)
        and_A_xorAB = np.logical_and(equal_fault[i,i+1:], xor_AB) # AND(A,XOR(A,B))
        and_B_xorAB = np.logical_and(equal_cluster[i,i+1:], xor_AB) # AND(B,XOR(A,B))
        
        numTP += np.sum(and_AB)
        numTN += np.sum(nor_AB)
        numFP += np.sum(and_B_xorAB)
        numFN += np.sum(and_A_xorAB)
    
    if (numTP+numTN+numFP+numFN) != ((numPoints*(numPoints-1))/2):
        print('Something is wrong',numTP,numTN,numFP,numFN,numPoints)
    
    return numTP,numTN,numFP,numFN

def calc_validation_indexes(Data, faults, labels):
    """Calculate validation indexes for given faults and labels
    Returns precision, recall, Rand index, F1, adjusted Rand index and silhouette score"""
    numPoints = labels.shape[0]
        
    numTP,numTN,numFP,numFN = calc_confusion_matrix(numPoints, faults, labels)
    
    precision = numTP/(numTP+numFP)
    recall = numTP/(numTP+numFN)
    rand = (numTP+numTN)/((numPoints*(numPoints-1))/2)
    f1 = 2*((precision*recall)/(precision+recall))
    ari = adjusted_rand_score(faults, labels)
    silhouette = silhouette_score(Data, labels)
    
    return precision,recall,rand,f1,ari,silhouette

def remove_irrelevant_clusters(labels):
    """Replaces all clusters that have less than 30 elements with label -1 (marks as irrelevant)"""
    _,counts = np.unique(labels, return_counts=True)
    for i in range(labels.shape[0]):
        if (counts[labels[i]] < 30):
            labels[i] = -1
    return labels
    

def K_mean(Data, faults):
    """Uses K-Mean clustering algorithm to cluster the data"""
    
    ks = []
    validIndexes = {'precision': [], 'recall': [], 'rand': [], 'f1': [], 'ari': [], 'silhouette': []}
    
    # Iterate through the k values
    min_k = 2
    max_k = int((np.unique(faults).shape[0] + int(np.sqrt(Data.shape[0])))/2) # average between numFaults and sqrt of numPoints
    for k in range(min_k, max_k+1):
        # Cluster using kmeans
        kmeans = cluster.KMeans(n_clusters=k).fit(Data)
        labels = kmeans.predict(Data)
        
        # Remove irrelevant clusters (group them under the -1 label, just like the faults are)
        labels = remove_irrelevant_clusters(labels)
        
        # Determine validation indexes
        precision,recall,rand,f1,ari,silhouette = calc_validation_indexes(Data, faults, labels)
        
        # Graph stuff
        ks.append(k)
        validIndexes['precision'].append(precision)
        validIndexes['recall'].append(recall)
        validIndexes['rand'].append(rand)
        validIndexes['f1'].append(f1)
        validIndexes['ari'].append(ari)
        validIndexes['silhouette'].append(silhouette)
        print('K:',k,'Precision:',precision,'Recall:',recall,'Rand:',rand,'F1:',f1,'ARI:',ari,'Silhouette',silhouette)
    
    # Plot graph of the validations indexes
    graph_validation_indexes(ks,'K',validIndexes,5,'kmean_val.png')
    
    # Plot clusters for best K
    best_k = int(input('Insert best K to plot clusters > '))
    kmeans = cluster.KMeans(n_clusters=best_k).fit(Data)
    labels = kmeans.predict(Data)
    labels = remove_irrelevant_clusters(labels)
    plot_classes(labels, long, lat, 'kmean_map_k'+str(best_k)+'.png')

def gmm(Data, faults):
    """Uses Gaussian Mixture Model clustering algorithm to cluster the data"""
    
    cs = []
    validIndexes = {'precision': [], 'recall': [], 'rand': [], 'f1': [], 'ari': [], 'silhouette': []}
    
    # Iterate through the c values
    min_c = 2
    max_c = int((np.unique(faults).shape[0] + int(np.sqrt(Data.shape[0])))/2) # average between numFaults and sqrt of numPoints
    for c in range(min_c, max_c+1):
        # Cluster using GMM
        gmm = mixture.GaussianMixture(n_components=c).fit(Data)
        labels = gmm.predict(Data)
        
        # Remove irrelevant clusters (group them under the -1 label, just like the faults are)
        labels = remove_irrelevant_clusters(labels)
    
        # Determine validation indexes
        precision,recall,rand,f1,ari,silhouette = calc_validation_indexes(Data, faults, labels)
        
        # Graph stuff
        cs.append(c)
        validIndexes['precision'].append(precision)
        validIndexes['recall'].append(recall)
        validIndexes['rand'].append(rand)
        validIndexes['f1'].append(f1)
        validIndexes['ari'].append(ari)
        validIndexes['silhouette'].append(silhouette)
        print('C:',c,'Precision:',precision,'Recall:',recall,'Rand:',rand,'F1:',f1,'ARI:',ari,'Silhouette',silhouette)
    
    # Plot graph of the validations indexes
    graph_validation_indexes(cs,'C',validIndexes,5,'gmm_val.png')
    
    # Plot clusters for best C
    best_c = int(input('Insert best number of components to plot clusters > '))
    gmm = mixture.GaussianMixture(n_components=best_c).fit(Data)
    labels = gmm.predict(Data)
    labels = remove_irrelevant_clusters(labels)
    plot_classes(labels, long, lat, 'gmm_map_c'+str(best_c)+'.png')

def dbscan(Data,faults):
    """Uses DBScan clustering algorithm to cluster the data"""
    
    es = []
    validIndexes = {'precision': [], 'recall': [], 'rand': [], 'f1': [], 'ari': [], 'silhouette': []}
    
    # Iterate through the e values
    min_e = 50
    max_e = 2000
    for e in range(min_e, max_e+1, 50):
        # Cluster using DBScan
        labels = cluster.DBSCAN(eps=e, min_samples=4).fit_predict(Data)
    
        # Determine validation indexes
        precision,recall,rand,f1,ari,silhouette = calc_validation_indexes(Data, faults, labels)
        
        # Graph stuff
        es.append(e)
        validIndexes['precision'].append(precision)
        validIndexes['recall'].append(recall)
        validIndexes['rand'].append(rand)
        validIndexes['f1'].append(f1)
        validIndexes['ari'].append(ari)
        validIndexes['silhouette'].append(silhouette)
        print('ε:',e,'Precision:',precision,'Recall:',recall,'Rand:',rand,'F1:',f1,'ARI:',ari,'Silhouette',silhouette)
    
    # Plot graph of the validations indexes
    graph_validation_indexes(es,'ε',validIndexes,5,'dbscan_val.png')
    
    # Plot clusters for best e
    best_e = int(input('Insert best ε to plot clusters > '))
    labels = cluster.DBSCAN(eps=best_e, min_samples=4).fit_predict(Data)
    plot_classes(labels, long, lat, 'dbscan_map_e'+str(best_e)+'.png')

def dbscan_paper(Data,faults):
    
    es = []
    validIndexes = {'precision': [], 'recall': [], 'rand': [], 'f1': [], 'ari': [], 'silhouette': []}
    
    
    knn = KNeighborsClassifier(n_neighbors=4)
    #print(np.zeros(Data[:,0].shape).size)
    knn.fit(Data,np.zeros(Data[:,0].shape))
    #get distances of the 4 nearest points
    dist,ind=knn.kneighbors()
    
    #select the 4th distance
    dist=dist[:,3]
    print(dist)
    #sort distances
    dist=np.sort(dist)
    dist=dist[::-1]
    plt.figure(figsize=(20,10),frameon=False)
    plt.plot(dist)
    plt.ylabel('distance')
    plt.yticks(np.arange(0,np.ceil(max(dist))+1,50))
    plt.grid(b=True, which='major', axis='both')
    plt.savefig('dbscan_paper_knn.png',dpi=300)
    plt.show()
    
    x=0
    
    while x <=600:#TODO choose e range
        labels = cluster.DBSCAN(eps=dist[x],min_samples=4).fit_predict(Data)
        precision,recall,rand,f1,ari,silhouette = calc_validation_indexes(Data, faults, labels)
        
        # Graph stuff
        es.append(dist[x])
        validIndexes['precision'].append(precision)
        validIndexes['recall'].append(recall)
        validIndexes['rand'].append(rand)
        validIndexes['f1'].append(f1)
        validIndexes['ari'].append(ari)
        validIndexes['silhouette'].append(silhouette)
        print('ε:',dist[x],'Precision:',precision,'Recall:',recall,'Rand:',rand,'F1:',f1,'ARI:',ari,'Silhouette',silhouette)
        x+=20
    
     # Plot graph of the validations indexes
    graph_validation_indexes(es,'e',validIndexes,10,'dbscan_paper_val.png')
    
    plot_classes(labels, long, lat, 'dbscan_paper_map.png')
    


# Load data
data = pd.read_csv('tp2_data.csv')
lat = data.latitude
long = data.longitude
faults = data.fault

# Plot points according to faults
#plot_classes(faults, long, lat)

# Convert lat,long to Earth-centered, Earth-fixed coords
x,y,z = lat_long_to_3D(lat, long)
Data = np.zeros((long.shape[0],3))
Data[:,0] = x
Data[:,1] = y
Data[:,2] = z

#plot 3D
'''
fig =plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(x,y,z)
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')
plt.show
'''

# Faults map
plot_classes(faults, long, lat, 'faults_map.png')

# Cluster using K-means
K_mean(Data, faults)

# CLuster using Gaussian Mixture Models
gmm(Data,faults)

# Cluster using DBScan
dbscan(Data,faults)

# Cluster using DBScan but selecting the parameter acording to method in the paper
dbscan_paper(Data,faults)
